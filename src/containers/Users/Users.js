import React, { useEffect } from "react";
import LayoutWrapper from "@iso/components/utility/layoutWrapper";
import PageHeader from "@iso/components/utility/pageHeader";
import LayoutContent from "@iso/components/utility/layoutContent";
import UserStyleWrapper from "@iso/containers/Users/User.styles";
import { Row, Tag } from "antd";

import userActions from "@iso/redux/user/actions";
import { useDispatch, useSelector } from "react-redux";
import MaterialTable from "material-table";

const Users = () => {
  const dispatch = useDispatch();
  const { users, loading, totalUser } = useSelector((state) => state.User);

  useEffect(() => {
    dispatch(userActions.getUsers());
  }, [dispatch]);

  return (
    <LayoutWrapper>
      <PageHeader>Người dùng</PageHeader>
      <LayoutContent>
        <UserStyleWrapper>
          <Row style={{ marginBottom: 10 }}>
            <span style={{ fontSize: 16 }}>Tổng số tài khoản: {totalUser}</span>
          </Row>
          <Row>
            <MaterialTable
              style={{ width: "100%" }}
              isLoading={loading}
              title="Danh sách người dùng"
              columns={[
                {
                  title: "Tên người dùng",
                  field: "displayName",
                },
                {
                  title: "Ảnh đại diện",
                  field: "photoURL",
                  // eslint-disable-next-line react/display-name
                  render: (rowData) => (
                    <img
                      src={rowData.photoURL}
                      alt=""
                      style={{ width: 40, borderRadius: 50 }}
                    />
                  ),
                  filtering: false,
                  sorting: false,
                },
                {
                  title: "Email",
                  field: "email",
                },
                {
                  title: "Trạng thái",
                  field: "status",
                  // eslint-disable-next-line react/display-name
                  render: (rowData) => (
                    <Tag color={rowData.status === "ACTIVE" ? "green" : "volcano"}>
                      {rowData.status}
                    </Tag>
                  ),
                },
                {
                  title: "Kiểu đăng nhập",
                  field: "providerId",
                },
                {
                  title: "Quyền",
                  field: "isAdmin",
                  // eslint-disable-next-line react/display-name
                  render: (rowData) => <div>{rowData.role}</div>,
                },
              ]}
              data={users}
              options={{
                actionsColumnIndex: 0,
                pageSize: 5,
                filtering: true,
              }}
            />
          </Row>
        </UserStyleWrapper>
      </LayoutContent>
    </LayoutWrapper>
  );
};

export default Users;
