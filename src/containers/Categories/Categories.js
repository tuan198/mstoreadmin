import React, { useCallback, useEffect } from "react";
import { Row, Col, Card, Button, message, Modal } from "antd";
import { useHistory } from "react-router";
import { Link, useRouteMatch } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import categoryActions from "@iso/redux/category/actions";
import {
  EditOutlined,
  DeleteOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import Loader from "@iso/components/utility/loader";

import LayoutWrapper from "@iso/components/utility/layoutWrapper";
import PageHeader from "@iso/components/utility/pageHeader";
import CategoryStyleWrapper from "./Categories.styles";

const { Meta } = Card;
const { confirm } = Modal;

const Categories = () => {
  const history = useHistory();
  const { url } = useRouteMatch();
  const dispatch = useDispatch();
  const { categories, loading, totalCategory } = useSelector(
    (state) => state.Category
  );

  useEffect(() => {
    dispatch(categoryActions.getCategories());
  }, [dispatch]);

  const onDelete = useCallback(
    (categoryId) => {
      new Promise((resolve, reject) => {
        dispatch(categoryActions.deleteCategory({ categoryId, resolve, reject }));
      })
        .then(() => message.success("Xóa thành công"))
        .catch((e) => message.error(`Lỗi ${e.message}`));
    },
    [dispatch]
  );

  const showDeleteConfirm = ({ categoryId, name }) => {
    confirm({
      title: "Bạn có muốn xóa danh mục này?",
      icon: <ExclamationCircleOutlined />,
      content: name,
      okText: "Có",
      okType: "danger",
      cancelText: "Không",
      onOk() {
        onDelete(categoryId);
      },
      onCancel() {},
    });
  };

  return (
    <LayoutWrapper>
      <PageHeader>Danh mục</PageHeader>
      <CategoryStyleWrapper>
        {loading ? (
          <Loader />
        ) : (
          <>
            <Row justify="end" className="ptb-5">
              <Col>
                <Button type="primary" onClick={() => history.push(`${url}/add`)}>
                  Thêm danh mục
                </Button>
              </Col>
            </Row>
            <Row style={{ marginBottom: 10 }}>
              <span style={{ fontSize: 16 }}>Tổng số danh mục: {totalCategory}</span>
            </Row>
            <div className="site-card-wrapper">
              <Row gutter={16}>
                {categories.map((category, index) => (
                  <Col key={index} xs={24} sm={12} md={8} className="ptb-5">
                    <Card
                      loading={loading}
                      cover={
                        <img
                          style={{ height: 300 }}
                          alt={category.name}
                          src={category.image}
                        />
                      }
                      actions={[
                        <Link key={index} to={`${url}/edit/${category.id}`}>
                          <EditOutlined key="edit" /> Chỉnh sửa
                        </Link>,
                        <div
                          key="delete"
                          onClick={() =>
                            showDeleteConfirm({
                              categoryId: category.id,
                              name: category.name,
                            })
                          }
                        >
                          <DeleteOutlined /> Xóa
                        </div>,
                      ]}
                    >
                      <Meta title={category.name} />
                    </Card>
                  </Col>
                ))}
              </Row>
            </div>
          </>
        )}
      </CategoryStyleWrapper>
    </LayoutWrapper>
  );
};

export default Categories;
