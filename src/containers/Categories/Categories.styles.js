import styled from "styled-components";
import { palette } from "styled-theme";

const CategoryStyleWrapper = styled.div`
  width: 100%;
  padding: 25px 35px;
  background-color: #f7f9fc;
  border: 1px solid ${palette("border", 0)};
  height: 100%;

  .site-card-border-less-wrapper {
    padding: 30px;
    background: #ececec;
  }

  .ptb-5 {
    padding: 5px 0;
  }
`;

export default CategoryStyleWrapper;
