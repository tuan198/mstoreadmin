import React, { useCallback, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { Formik } from "formik";
import { Form, Input } from "formik-antd";
import { Modal, Checkbox, Button } from "antd";
import authAction from "@iso/redux/auth/actions";
import SignInStyleWrapper from "./SignIn.styles";
import validationSchema from "@iso/containers/SignIn/Validation.schema";
import Loader from "@iso/components/utility/loader";

export default function SignIn() {
  let history = useHistory();
  const dispatch = useDispatch();
  const { idToken, error, loading } = useSelector((state) => state.Auth);

  useEffect(() => {
    if (idToken) {
      history.push("/dashboard");
    }
  }, [idToken, history]);

  const handleLogin = useCallback(
    (data) => {
      const { email, password } = data;
      dispatch(authAction.login({ email, password }));
    },
    [dispatch]
  );

  useEffect(() => {
    if (error) {
      Modal.error({
        title: `Lỗi ${error}`,
        content: "Đăng nhập không thành công",
        onOk: () => {},
      });
    }
  }, [error, dispatch]);

  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <SignInStyleWrapper className="isoSignInPage">
          <div className="isoLoginContentWrapper">
            <div className="isoLoginContent">
              <div className="isoLogoWrapper">MSTORE ADMIN</div>
              <div className="isoSignInForm">
                <Formik
                  initialValues={initialValues}
                  onSubmit={handleLogin}
                  validationSchema={validationSchema()}
                >
                  <Form {...layout} colon={false} labelAlign="left">
                    <Form.Item
                      className="isoInputWrapper"
                      name="email"
                      label="Email"
                    >
                      <Input autoComplete="new-password" name="email" />
                    </Form.Item>
                    <Form.Item
                      className="isoInputWrapper"
                      name="password"
                      label="Mật khẩu"
                    >
                      <Input.Password autoComplete="new-password" name="password" />
                    </Form.Item>
                    <div className="isoInputWrapper isoLeftRightComponent">
                      <Checkbox>Nhớ mật khẩu</Checkbox>
                      <Button type="primary" htmlType="submit">
                        Đăng nhập
                      </Button>
                    </div>
                  </Form>
                </Formik>
                <div className="isoCenterComponent isoHelperWrapper">
                  <Link to="/forgotpassword" className="isoForgotPass">
                    Quyên mật khẩu
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </SignInStyleWrapper>
      )}
    </>
  );
}

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};

const initialValues = {
  email: null,
  password: null,
};
