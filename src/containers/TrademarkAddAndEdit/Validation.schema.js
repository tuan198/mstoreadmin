import Yup from "@iso/config/validation.config";

const validationSchema = () =>
  Yup.object().shape({
    name: Yup.string().required().nullable().label("Tên hãng sản xuất"),
    description: Yup.string().required().nullable().label("Mô tả"),
  });

export default validationSchema;
