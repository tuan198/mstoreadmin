import styled from "styled-components";

const TrademarkAddStyleWrapper = styled.div`
  .ant-form-item-label > label.ant-form-item-required::after {
    display: inline-block;
    margin-right: 4px;
    color: #ff4d4f;
    font-size: 14px;
    font-family: SimSun, sans-serif;
    line-height: 1;
    content: "*";
  }

  .ant-form-item-label > label.ant-form-item-required::before {
    margin-right: 0px;
    content: "";
  }
`;

export default TrademarkAddStyleWrapper;
