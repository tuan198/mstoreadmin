import React, { useEffect } from "react";
import { Link, useHistory, useRouteMatch } from "react-router-dom";
import { Button, Col, Row } from "antd";
import MaterialTable from "material-table";
import { Edit } from "@material-ui/icons";
import { useDispatch, useSelector } from "react-redux";
import productActions from "@iso/redux/product/actions";

import LayoutWrapper from "@iso/components/utility/layoutWrapper";
import PageHeader from "@iso/components/utility/pageHeader";
import LayoutContent from "@iso/components/utility/layoutContent";
import ProductStyleWrapper from "@iso/containers/Products/Products.styles";

const Products = () => {
  const history = useHistory();
  const { url } = useRouteMatch();
  const dispatch = useDispatch();
  const { products, loading, totalProduct } = useSelector((state) => state.Product);

  useEffect(() => {
    dispatch(productActions.getProducts());
  }, [dispatch]);

  return (
    <LayoutWrapper>
      <PageHeader>Sản phẩm</PageHeader>
      <LayoutContent>
        <ProductStyleWrapper>
          <Row justify="end">
            <Col>
              <Button type="primary" onClick={() => history.push(`${url}/add`)}>
                Thêm sản phẩm
              </Button>
            </Col>
          </Row>
          <br />
          <Row style={{ marginBottom: 10 }}>
            <span style={{ fontSize: 16 }}>Tổng số sản phẩm: {totalProduct}</span>
          </Row>
          <Row>
            <MaterialTable
              style={{ width: "100%" }}
              isLoading={loading}
              title="Danh sách sản phẩm"
              columns={[
                {
                  title: "Chỉnh sửa",
                  field: "id",
                  // eslint-disable-next-line react/display-name
                  render: (rowData) => (
                    <Link to={`${url}/edit/${rowData.id}`}>
                      <Edit />
                    </Link>
                  ),
                  filtering: false,
                  sorting: false,
                },
                {
                  title: "Tên  sản phẩm",
                  field: "name",
                  // eslint-disable-next-line react/display-name
                  render: (rowData) => (
                    <div style={{ minWidth: 300 }}>{rowData.name}</div>
                  ),
                },
                {
                  title: "Hình ảnh",
                  field: "productImage",
                  // eslint-disable-next-line react/display-name
                  render: (rowData) => (
                    <img
                      src={rowData.productImage}
                      alt=""
                      style={{ width: 60, borderRadius: "5" }}
                    />
                  ),
                  filtering: false,
                  sorting: false,
                },
                {
                  title: "Hãng sản xuất",
                  field: "trademark",
                },
                {
                  title: "Phân loại",
                  field: "category",
                },
                {
                  title: "Giá bán (VNĐ)",
                  field: "price",
                  // eslint-disable-next-line react/display-name
                  render: (rowData) => (
                    <div style={{ minWidth: 110 }}>{rowData.price}</div>
                  ),
                },
                {
                  title: "Giá gốc (VNĐ)",
                  field: "oldPrice",
                  // eslint-disable-next-line react/display-name
                  render: (rowData) => (
                    <div style={{ minWidth: 130 }}>{rowData.oldPrice}</div>
                  ),
                },
                {
                  title: "Số lượng",
                  field: "amount",
                  type: "numeric",
                  filtering: false,
                  sorting: false,
                },
              ]}
              data={products}
              options={{
                actionsColumnIndex: 0,
                pageSize: 5,
                filtering: true,
              }}
            />
          </Row>
        </ProductStyleWrapper>
      </LayoutContent>
    </LayoutWrapper>
  );
};

export default Products;
