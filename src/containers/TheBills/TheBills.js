import React, { useEffect, useState } from "react";
import { Row, Tag, Tooltip } from "antd";
import { Link, useRouteMatch } from "react-router-dom";
import { ViewHeadline } from "@material-ui/icons";
import MaterialTable from "material-table";

import LayoutWrapper from "@iso/components/utility/layoutWrapper";
import PageHeader from "@iso/components/utility/pageHeader";
import LayoutContent from "@iso/components/utility/layoutContent";
import TheBillStyleWrapper from "@iso/containers/TheBills/TheBills.styles";
import { firebaseApp } from "@iso/config/firebase.config";

const TheBills = () => {
  const [data, setData] = useState([]);
  const { url } = useRouteMatch();
  const [loading, setLoading] = useState(true);
  const [totalInvoices, setTotalInvoices] = useState(0);

  useEffect(() => {
    const List = [];
    firebaseApp
      .firestore()
      .collection("invoices")
      .where("status", "in", ["Đang xử lý", "Đã hủy", "Hoàn thành"])
      .get()
      .then((querySnapshot) => {
        setTotalInvoices(querySnapshot.size);
        querySnapshot.forEach((doc) => {
          const list = doc.data();
          List.push({
            id: doc.id,
            ...list,
          });
        });
      })
      .then(() => {
        setData(List);
        setLoading(false);
      })
      .catch(() => setLoading(false));
  }, []);

  return (
    <LayoutWrapper>
      <PageHeader>Đơn hàng</PageHeader>
      <LayoutContent>
        <TheBillStyleWrapper>
          <Row style={{ marginBottom: 10 }}>
            <span style={{ fontSize: 16 }}>Tổng số đơn hàng: {totalInvoices}</span>
          </Row>
          <Row>
            <MaterialTable
              style={{ width: "100%" }}
              isLoading={loading}
              title="Danh sách đơn hàng"
              columns={[
                {
                  title: "Chỉnh sửa",
                  field: "id",
                  // eslint-disable-next-line react/display-name
                  render: (rowData) => (
                    <div>
                      <Link to={`${url}/detail/${rowData.id}`}>
                        <Tooltip title="Chi tiết" color="cyan">
                          <ViewHeadline />
                        </Tooltip>
                      </Link>
                    </div>
                  ),
                  filtering: false,
                  sorting: false,
                },
                {
                  title: "Tên người dùng",
                  field: "name",
                },
                {
                  title: "Email",
                  field: "email",
                },
                {
                  title: "Trạng thái",
                  field: "status",
                  // eslint-disable-next-line react/display-name
                  render: (rowData) => (
                    <Tag
                      color={
                        rowData.status === "Chờ xử lý"
                          ? "#fff566"
                          : rowData.status === "Đang xử lý"
                          ? "#1890ff"
                          : rowData.status === "Hoàn thành"
                          ? "#389e0d"
                          : "#f5222d"
                      }
                    >
                      {rowData.status.toUpperCase()}
                    </Tag>
                  ),
                },
                {
                  title: "Ngày tạo",
                  field: "createDate",
                },
                {
                  title: "Số điện thoại",
                  field: "phoneNumber",
                },
              ]}
              data={data}
              options={{
                actionsColumnIndex: 0,
                pageSize: 5,
                filtering: true,
              }}
            />
          </Row>
        </TheBillStyleWrapper>
      </LayoutContent>
    </LayoutWrapper>
  );
};

export default TheBills;
