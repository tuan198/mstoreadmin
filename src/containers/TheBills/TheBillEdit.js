import React, { useCallback, useEffect, useState } from "react";
import { Row, Button, message } from "antd";
import { useDispatch, useSelector } from "react-redux";
import orderActions from "@iso/redux/order/actions";
import MaterialTable from "material-table";
import { useParams } from "react-router";
import { isEmpty } from "lodash";
// import { useReactToPrint } from "react-to-print";

import LayoutWrapper from "@iso/components/utility/layoutWrapper";
import PageHeader from "@iso/components/utility/pageHeader";
import LayoutContent from "@iso/components/utility/layoutContent";
import TheBillStyleWrapper from "@iso/containers/TheBills/TheBills.styles";
import { Formik } from "formik";
import { Select, Form } from "formik-antd";
// import { firebaseApp } from "@iso/config/firebase.config";
//
// class ComponentToPrint extends React.Component {
//   render() {
//     return <div>{this.props.name}</div>;
//   }
// }

const TheBillEdit = () => {
  // const componentRef = useRef();
  const { orderId } = useParams();
  const dispatch = useDispatch();
  const { order, loading } = useSelector((state) => state.Order);
  const [initialValues, setInitialValues] = useState(initialValueDefault);

  useEffect(() => {
    dispatch(orderActions.getOrder({ orderId: orderId }));
  }, [dispatch, orderId]);

  useEffect(() => {
    if (!isEmpty(order)) {
      setInitialValues({ status: order.status });
    }
  }, [order]);

  const handleSubmit = useCallback(
    (data, form) => {
      new Promise((resolve, reject) => {
        return dispatch(
          orderActions.changeOrderStatus({
            status: data.status,
            orderId,
            resolve,
            reject,
          })
        );
      })
        .then(() => {
          message.success("Đã cập nhật trạng thái");
          form.setSubmitting(false);
        })
        .catch(() => {
          message.error("Lỗi");
          form.setSubmitting(false);
        });
    },
    [dispatch, orderId]
  );

  // const printReceipt = useReactToPrint({
  //   content: () => componentRef.current,
  // });

  return (
    <LayoutWrapper>
      <PageHeader>Chi tiết đơn hàng</PageHeader>
      <LayoutContent>
        <TheBillStyleWrapper>
          <Row justify="end" style={{ marginBottom: 20 }}>
            <Formik
              initialValues={initialValues}
              onSubmit={handleSubmit}
              enableReinitialize={true}
              render={({ isSubmitting, dirty }) => (
                <Form colon={false} layout="inline">
                  <Form.Item name="status" label="Thay đổi trạng thái">
                    <Select name="status" style={{ width: 200 }}>
                      <Select.Option value="Chờ xử lý">Chờ xử lý</Select.Option>
                      <Select.Option value="Đang xử lý">Đang xử lý</Select.Option>
                      <Select.Option value="Hoàn thành">Hoàn thành</Select.Option>
                      <Select.Option value="Đã hủy">Hủy</Select.Option>
                    </Select>
                  </Form.Item>
                  <Button
                    htmlType="submit"
                    type="primary"
                    disabled={!dirty}
                    loading={isSubmitting}
                  >
                    Lưu
                  </Button>
                  {/*<Button*/}
                  {/*  onClick={printReceipt}*/}
                  {/*  style={{ marginLeft: 20 }}*/}
                  {/*  type="primary"*/}
                  {/*>*/}
                  {/*  In đơn hàng*/}
                  {/*</Button>*/}
                </Form>
              )}
            />
          </Row>
          <Row style={{ flexDirection: "column", marginBottom: 20 }}>
            <span style={{ fontSize: 16, fontWeight: "bold" }}>
              Tên người mua: {order.name}
            </span>
            <span style={{ fontSize: 16, fontWeight: "bold" }}>
              Địa chỉ mail: {order.email}
            </span>
            <span style={{ fontSize: 16, fontWeight: "bold" }}>
              Số điện thoại: {order.phoneNumber}
            </span>
            <span style={{ fontSize: 16, fontWeight: "bold" }}>
              Địa chỉ giao hàng: {order.address}
            </span>
            <span style={{ fontSize: 16, fontWeight: "bold" }}>
              Ngày tạo: {order.createDate}
            </span>
            <span style={{ fontSize: 16, fontWeight: "bold" }}>
              Tổng tiền: {order.totalMoney}
            </span>
          </Row>
          <Row>
            <MaterialTable
              style={{ width: "100%" }}
              isLoading={loading}
              title="Danh sách sản phẩm"
              columns={[
                {
                  title: "Tên  sản phẩm",
                  field: "name",
                  // eslint-disable-next-line react/display-name
                  render: (rowData) => (
                    <div style={{ minWidth: 300 }}>{rowData.name}</div>
                  ),
                },
                {
                  title: "Hình ảnh",
                  field: "productImage",
                  // eslint-disable-next-line react/display-name
                  render: (rowData) => (
                    <img
                      src={rowData.productImage}
                      alt=""
                      style={{ width: 100, borderRadius: "5" }}
                    />
                  ),
                  filtering: false,
                  sorting: false,
                },
                {
                  title: "Hãng sản xuất",
                  field: "trademark",
                },
                {
                  title: "Phân loại",
                  field: "category",
                },
                {
                  title: "Giá bán (VNĐ)",
                  field: "price",
                  // eslint-disable-next-line react/display-name
                  render: (rowData) => (
                    <div style={{ minWidth: 110 }}>{rowData.price}</div>
                  ),
                },
                {
                  title: "Số lượng",
                  field: "quantum",
                  type: "numeric",
                  filtering: false,
                  sorting: false,
                },
              ]}
              data={order.products}
              options={{
                actionsColumnIndex: 0,
                pageSize: 5,
                filtering: true,
              }}
            />
          </Row>
          {/*<div style={{ display: "none" }}>*/}
          {/*  <ComponentToPrint ref={componentRef} props={order} />*/}
          {/*</div>*/}
        </TheBillStyleWrapper>
      </LayoutContent>
    </LayoutWrapper>
  );
};

const initialValueDefault = {
  status: null,
};

export default TheBillEdit;
