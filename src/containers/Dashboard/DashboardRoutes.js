import React, { lazy, Suspense } from "react";
import { Route, useRouteMatch, Switch } from "react-router-dom";
import Loader from "@iso/components/utility/loader";

const routes = [
  {
    path: "",
    component: lazy(() => import("@iso/containers/BlankPage/BlankPage")),
    exact: true,
  },
  {
    path: "my-profile",
    component: lazy(() => import("@iso/containers/Profile/Profile")),
    exact: true,
  },
  {
    path: "product",
    component: lazy(() => import("@iso/containers/Products/Products")),
    exact: true,
  },
  {
    path: "product/add",
    component: lazy(() =>
      import("@iso/containers/ProductAddAndEdit/ProductAddAndEdit")
    ),
    exact: true,
  },
  {
    path: "product/edit/:productId",
    component: lazy(() =>
      import("@iso/containers/ProductAddAndEdit/ProductAddAndEdit")
    ),
    exact: true,
  },
  {
    path: "category",
    component: lazy(() => import("@iso/containers/Categories/Categories")),
    exact: true,
  },
  {
    path: "category/add",
    component: lazy(() =>
      import("@iso/containers/CategoryAddAndEdit/CategoryAddAndEdit")
    ),
    exact: true,
  },
  {
    path: "category/edit/:categoryId",
    component: lazy(() =>
      import("@iso/containers/CategoryAddAndEdit/CategoryAddAndEdit")
    ),
    exact: true,
  },
  {
    path: "trademark",
    component: lazy(() => import("@iso/containers/Trademarks/Trademarks")),
    exact: true,
  },
  {
    path: "trademark/add",
    component: lazy(() =>
      import("@iso/containers/TrademarkAddAndEdit/TrademarkAddAndEdit")
    ),
    exact: true,
  },
  {
    path: "trademark/edit/:trademarkId",
    component: lazy(() =>
      import("@iso/containers/TrademarkAddAndEdit/TrademarkAddAndEdit")
    ),
    exact: true,
  },
  {
    path: "the-bill",
    component: lazy(() => import("@iso/containers/TheBills/TheBills")),
    exact: true,
  },
  {
    path: "the-bill/detail/:orderId",
    component: lazy(() => import("@iso/containers/TheBills/TheBillEdit")),
    exact: true,
  },
  {
    path: "user",
    component: lazy(() => import("@iso/containers/Users/Users")),
    exact: true,
  },
];

export default function AppRouter() {
  const { url } = useRouteMatch();
  return (
    <Suspense fallback={<Loader />}>
      <Switch>
        {routes.map((route, idx) => (
          <Route exact={route.exact} key={idx} path={`${url}/${route.path}`}>
            <route.component />
          </Route>
        ))}
      </Switch>
    </Suspense>
  );
}
