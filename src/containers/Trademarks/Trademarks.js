import React, { useCallback, useEffect } from "react";
import { Row, Col, Card, Button, message, Modal } from "antd";
import { useHistory } from "react-router";
import { Link, useRouteMatch } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import trademarkActions from "@iso/redux/trademark/actions";
import {
  DeleteOutlined,
  EditOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import Loader from "@iso/components/utility/loader";

import LayoutWrapper from "@iso/components/utility/layoutWrapper";
import PageHeader from "@iso/components/utility/pageHeader";
import TrademarkStyleWrapper from "@iso/containers/Trademarks/Trademark.styles";

const { Meta } = Card;
const { confirm } = Modal;

const Trademarks = () => {
  const history = useHistory();
  const { url } = useRouteMatch();
  const dispatch = useDispatch();
  const { trademarks, loading, totalTrademark } = useSelector(
    (state) => state.Trademark
  );

  useEffect(() => {
    dispatch(trademarkActions.getTrademarks());
  }, [dispatch]);

  const onDelete = useCallback(
    (trademarkId) => {
      new Promise((resolve, reject) => {
        dispatch(trademarkActions.deleteTrademark({ trademarkId, resolve, reject }));
      })
        .then(() => message.success("Xóa thành công"))
        .catch((e) => message.error(`Lỗi ${e.message}`));
    },
    [dispatch]
  );

  const showDeleteConfirm = ({ trademarkId, name }) => {
    confirm({
      title: "Bạn có muốn xóa hãng sản xuất này?",
      icon: <ExclamationCircleOutlined />,
      content: name,
      okText: "Có",
      okType: "danger",
      cancelText: "Không",
      onOk() {
        onDelete(trademarkId);
      },
      onCancel() {},
    });
  };

  return (
    <LayoutWrapper>
      <PageHeader>Hãng sản xuất</PageHeader>
      <TrademarkStyleWrapper>
        {loading ? (
          <div style={{ height: "100vh" }}>
            <Loader />
          </div>
        ) : (
          <>
            <Row justify="end" className="ptb-5">
              <Col>
                <Button type="primary" onClick={() => history.push(`${url}/add`)}>
                  Thêm hãng sản xuất
                </Button>
              </Col>
            </Row>
            <Row style={{ marginBottom: 10 }}>
              <span style={{ fontSize: 16 }}>
                Tổng số hãng sản xuất: {totalTrademark}
              </span>
            </Row>
            <div className="site-card-wrapper">
              <Row gutter={16}>
                {trademarks.map((trademark, index) => (
                  <Col key={index} xs={12} sm={6} md={4} className="ptb-5">
                    <Card
                      loading={loading}
                      cover={
                        <img
                          style={{ height: 200 }}
                          alt={trademark.name}
                          src={trademark.image}
                        />
                      }
                      actions={[
                        <Link key={index} to={`${url}/edit/${trademark.id}`}>
                          <EditOutlined key="edit" /> Chỉnh sửa
                        </Link>,
                        <div
                          key="delete"
                          onClick={() =>
                            showDeleteConfirm({
                              trademarkId: trademark.id,
                              name: trademark.name,
                            })
                          }
                        >
                          <DeleteOutlined /> Xóa
                        </div>,
                      ]}
                    >
                      <Meta title={trademark.name} />
                    </Card>
                  </Col>
                ))}
              </Row>
            </div>
          </>
        )}
      </TrademarkStyleWrapper>
    </LayoutWrapper>
  );
};

export default Trademarks;
