const options = [
  {
    key: "",
    label: "Dashboard",
    leftIcon: "ion-ios-home",
  },
  {
    key: "product",
    label: "Sản phẩm",
    leftIcon: "ion-ios-cart",
  },
  {
    key: "category",
    label: "Danh mục",
    leftIcon: "ion-android-apps",
  },
  {
    key: "trademark",
    label: "Hãng sản xuất",
    leftIcon: "ion-settings",
  },
  {
    key: "the-bill",
    label: "Đơn hàng",
    leftIcon: "ion-ios-paper",
  },
  {
    key: "user",
    label: "Người dùng",
    leftIcon: "ion-person-stalker",
  },
];
export default options;
