import Yup from "@iso/config/validation.config";

const validationSchema = () =>
  Yup.object().shape({
    email: Yup.string().email().trim().required().label("Email"),
    password: Yup.string().trim().min(6).max(12).required().label("Mật khẩu"),
  });

export default validationSchema;
