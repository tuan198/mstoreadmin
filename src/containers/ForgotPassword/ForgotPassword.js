import React, { useCallback } from "react";
import { Link } from "react-router-dom";
// import { useSelector, useDispatch } from "react-redux";
import { Formik } from "formik";
import { Form, Input } from "formik-antd";
import { Button } from "antd";
// import authAction from "@iso/redux/auth/actions";
import ForgotPasswordStyleWrapper from "./ForgotPassword.styles";
import validationSchema from "@iso/containers/SignIn/Validation.schema";

export default function ForgotPassword() {
  // let history = useHistory();
  // const dispatch = useDispatch();
  // const { idToken, error, loading } = useSelector((state) => state.Auth);

  const handleSubmit = useCallback((data) => {
    // const { email } = data;
    console.log(data);
    // dispatch(authAction.login({ email, 'password' }));
  }, []);

  return (
    <ForgotPasswordStyleWrapper className="isoForgotPassPage">
      <div className="isoFormContentWrapper">
        <div className="isoFormContent">
          <div className="isoLogoWrapper">QUYÊN MẬT KHẨU</div>
          <div className="isoForgotPassForm">
            <Formik
              initialValues={initialValues}
              onSubmit={handleSubmit}
              validationSchema={validationSchema()}
            >
              <Form colon={false} labelAlign="left">
                <h3>Nhập email của bạn để lấy lại mật khẩu</h3>
                <br />
                <Form.Item className="isoInputWrapper" name="email">
                  <Input autoComplete="new-password" name="email" />
                </Form.Item>
                <br />
                <div className="isoInputWrapper isoLeftRightComponent">
                  <Button type="primary" htmlType="submit">
                    Gửi mail
                  </Button>
                </div>
              </Form>
            </Formik>
            <br />
            <div className="isoCenterComponent isoHelperWrapper">
              <Link to="/" className="isoForgotPass">
                Quay lại trang đăng nhập
              </Link>
            </div>
          </div>
        </div>
      </div>
    </ForgotPasswordStyleWrapper>
  );
}

// const layout = {
//   labelCol: { span: 6 },
//   wrapperCol: { span: 18 },
// };

const initialValues = {
  email: null,
};
