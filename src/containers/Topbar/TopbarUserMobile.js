import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Popover from "@iso/components/uielements/popover";
import TopbarDropdownWrapper from "./TopbarDropdown.styles";
import TopbarLogout from "./TopbarLogout";

const TopbarUserMobile = ({ user }) => {
  const [visible, setVisibility] = React.useState(false);
  const handleVisibleChange = () => {
    setVisibility((visible) => !visible);
  };

  const content = (
    <TopbarDropdownWrapper className="isoUserDropdown">
      <Link
        className="isoDropdownLink"
        to={"/dashboard/my-profile"}
        onClick={() => setVisibility(false)}
      >
        {user.displayName}
      </Link>
      <div className="isoDropdownLink" onClick={() => setVisibility(false)}>
        <TopbarLogout view="MobileView" />
      </div>
    </TopbarDropdownWrapper>
  );

  return (
    <Popover
      content={content}
      trigger="click"
      visible={visible}
      onVisibleChange={handleVisibleChange}
      arrowPointAtCenter={true}
      placement="bottomLeft"
    >
      <div className="isoImgWrapper">
        {/* <img alt="user" src={userpic} />
        <span className="userActivity online" /> */}
        <i className="ion-android-settings" />
      </div>
    </Popover>
  );
};

TopbarUserMobile.propTypes = {
  user: PropTypes.object,
  view: PropTypes.string,
};

export default TopbarUserMobile;
