import React from "react";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import ModalConfirm from "@iso/components/Feedback/ModalConfirm";
import authAction from "@iso/redux/auth/actions";

const TopbarLogout = ({ view }) => {
  const dispatch = useDispatch();
  const { logout } = authAction;
  return (
    <ModalConfirm
      content="Bạn có muốn đăng xuất không?"
      onOk={() => dispatch(logout())}
    >
      {view !== "MobileView" ? (
        <div className="isoIconWrapper">
          <i className="ion-log-out" />
        </div>
      ) : (
        <span>Logout</span>
      )}
    </ModalConfirm>
  );
};

TopbarLogout.propTypes = {
  view: PropTypes.string,
};

export default TopbarLogout;
