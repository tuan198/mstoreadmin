import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Layout } from "antd";
import appActions from "@iso/redux/app/actions";
import TopbarUser from "./TopbarUser";
import TopbarUserMobile from "./TopbarUserMobile";
import TopbarWrapper from "./Topbar.styles";

const { Header } = Layout;
const { toggleCollapsed } = appActions;

export default function Topbar() {
  const customizedTheme = useSelector((state) => state.ThemeSwitcher.topbarTheme);
  const { collapsed, openDrawer, view } = useSelector((state) => state.App);
  const { userData } = useSelector((state) => state.Auth);
  const dispatch = useDispatch();
  const handleToggle = React.useCallback(() => dispatch(toggleCollapsed()), [
    dispatch,
  ]);
  const isCollapsed = collapsed && !openDrawer;
  const styling = {
    background: customizedTheme.backgroundColor,
    position: "fixed",
    width: "100%",
    height: 70,
  };
  return (
    <TopbarWrapper>
      <Header
        style={styling}
        className={isCollapsed ? "isomorphicTopbar collapsed" : "isomorphicTopbar"}
      >
        <div className="isoLeft">
          <button
            className={
              isCollapsed ? "triggerBtn menuCollapsed" : "triggerBtn menuOpen"
            }
            style={{ color: customizedTheme.textColor }}
            onClick={handleToggle}
          />
        </div>
        <ul className="isoRight">
          {view !== "MobileView" ? (
            <li className="isoUser">
              <TopbarUser user={userData} />
            </li>
          ) : (
            <li>
              <TopbarUserMobile user={userData} />
            </li>
          )}
        </ul>
      </Header>
    </TopbarWrapper>
  );
}
