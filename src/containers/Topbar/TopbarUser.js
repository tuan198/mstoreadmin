import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import TopbarLogout from "./TopbarLogout";

const TopbarUser = ({ user }) => {
  return (
    <>
      <li>
        <Link
          className="isoDropdownLink"
          to={"/dashboard/my-profile"}
          style={{
            maxWidth: 150,
            display: "block",
            textOverflow: "ellipsis",
            overflow: "hidden",
            whiteSpace: "nowrap",
          }}
        >
          {user.displayName}
        </Link>
      </li>
      <li>
        <TopbarLogout view="DesktopView" />
      </li>
    </>
  );
};

TopbarUser.propTypes = {
  user: PropTypes.object,
};

export default TopbarUser;
