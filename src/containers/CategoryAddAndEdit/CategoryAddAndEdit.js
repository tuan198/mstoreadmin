import React, { useCallback, useEffect, useState, memo } from "react";
import { Row, Col, Upload, Button, notification, message } from "antd";
import { Formik } from "formik";
import { Form, Input } from "formik-antd";
import { PlusOutlined, LoadingOutlined } from "@ant-design/icons";
import { isEmpty } from "lodash";
import { useParams } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import categoryActions from "@iso/redux/category/actions";
import ImgCrop from "antd-img-crop";
import moment from "moment";

import CategoryAddStyleWrapper from "@iso/containers/CategoryAddAndEdit/CategoryAddAndEdit.styles";
import LayoutWrapper from "@iso/components/utility/layoutWrapper";
import LayoutContent from "@iso/components/utility/layoutContent";
import PageHeader from "@iso/components/utility/pageHeader";
import validationSchema from "@iso/containers/CategoryAddAndEdit/Validation.schema";
import { firebaseApp } from "@iso/config/firebase.config";

const CategoryAddAndEdit = () => {
  const { categoryId } = useParams();
  const dispatch = useDispatch();
  const { category } = useSelector((state) => state.Category);
  const [initialValues, setInitialValues] = useState(initialValue);
  const [imageUrl, setImageUrl] = useState(null);
  const [uploading, setUploading] = useState(false);
  const date = Date.now();

  useEffect(() => {
    if (categoryId) {
      dispatch(categoryActions.getCategory({ categoryId }));
    }
  }, [categoryId, dispatch]);

  useEffect(() => {
    if (categoryId && !isEmpty(category)) {
      setInitialValues(category);
      setImageUrl(category.image);
    }
  }, [category, categoryId]);

  const openNotification = (placement, message) => {
    notification.info({
      message: "Thông báo",
      description: message,
      placement,
    });
  };

  const handleSubmit = useCallback(
    (data, form) => {
      data.image = imageUrl;
      data.createDate = moment(date).format("DD/MM/YYYY HH:mm:ss");
      new Promise((resolve, reject) => {
        if (categoryId) {
          return dispatch(
            categoryActions.editCategory({
              categoryId,
              data,
              resolve,
              reject,
            })
          );
        }
        return dispatch(
          categoryActions.addCategory({
            data,
            resolve,
            reject,
          })
        );
      })
        .then(() => {
          openNotification("topRight", "Thêm thành công");
          form.setSubmitting(false);
        })
        .catch((e) => {
          openNotification("topRight", `Lỗi: ${e.message}`);
          form.setSubmitting(false);
        });
    },
    [categoryId, date, dispatch, imageUrl]
  );

  const getBase64 = (img, callback) => {
    // eslint-disable-next-line no-undef
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  };

  const onFileChange = (info) => {
    if (info.file.status === "uploading") {
      setUploading(true);
      return;
    }
    if (info.file.status === "done") {
      getBase64(info.file.originFileObj, (imageUrl) => {
        setImageUrl(imageUrl);
        setUploading(false);
      });
    }
  };

  const beforeUpload = (file) => {
    const isImage = file.type.indexOf("image/") === 0;
    if (!isImage) {
      message.error("Bạn chỉ được tải lên file hình ảnh");
    }
    const isLt5M = file.size / 1024 / 1024 < 5;
    if (!isLt5M) {
      message.error("Hình ảnh phải nhỏ hơn 5mb");
    }
    return isImage && isLt5M;
  };

  const customUpload = async ({ onError, onSuccess, file }) => {
    const storage = firebaseApp.storage();
    const metadata = {
      contentType: "image/jpeg",
    };
    const storageRef = await storage.ref();
    const imgFile = storageRef.child(`images/${file.name}`);
    try {
      const image = await imgFile.put(file, metadata);
      firebaseApp
        .storage()
        .ref("images")
        .child(file.name)
        .getDownloadURL()
        .then((fireBaseUrl) => {
          setImageUrl(fireBaseUrl);
        });
      onSuccess(null, image);
    } catch (e) {
      onError(e);
    }
  };

  return (
    <LayoutWrapper>
      <PageHeader>{categoryId ? "Sửa danh mục" : "Thêm danh mục"}</PageHeader>
      <LayoutContent>
        <CategoryAddStyleWrapper>
          <Formik
            onSubmit={handleSubmit}
            initialValues={initialValues}
            enableReinitialize={true}
            validationSchema={validationSchema}
            render={({ isSubmitting }) => (
              <Form {...layout} colon={false} labelAlign="left">
                <Row justify="end">
                  <Button type="primary" htmlType="submit" loading={isSubmitting}>
                    Lưu
                  </Button>
                </Row>
                <Row justify="center">
                  <Col xs={24} md={12}>
                    <Form.Item required name="name" label="Tên danh mục">
                      <Input name="name" />
                    </Form.Item>
                    <Form.Item required name="description" label="Mô tả">
                      <Input name="description" />
                    </Form.Item>
                    <Form.Item name="note" label="Ghi chú">
                      <Input name="note" />
                    </Form.Item>
                    <Form.Item name="image" label="Hình danh mục">
                      <ImgCrop rotate>
                        <Upload
                          name="avatar"
                          listType="picture-card"
                          className="avatar-uploader"
                          showUploadList={false}
                          beforeUpload={beforeUpload}
                          onChange={onFileChange}
                          customRequest={customUpload}
                        >
                          {imageUrl ? (
                            <img
                              src={imageUrl}
                              alt="image"
                              style={{ width: "100%" }}
                            />
                          ) : (
                            <div>
                              {uploading ? <LoadingOutlined /> : <PlusOutlined />}
                              <div className="ant-upload-text">Tải lên</div>
                            </div>
                          )}
                        </Upload>
                      </ImgCrop>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            )}
          />
        </CategoryAddStyleWrapper>
      </LayoutContent>
    </LayoutWrapper>
  );
};

const initialValue = {
  name: null,
  image: null,
  description: null,
  note: null,
  createDate: null,
};

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

export default memo(CategoryAddAndEdit);
