import styled from "styled-components";
import { palette } from "styled-theme";

const DashboardStyleWrapper = styled.div`
  width: 100%;
  padding: 10px;
  border: 1px solid ${palette("border", 0)};
  height: 100%;

  .layout {
    background-color: white;
    padding: 10px;
    border-radius: 5px;
  }
`;

export default DashboardStyleWrapper;
