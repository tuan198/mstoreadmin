import React, { useCallback, useEffect, useState } from "react";
import { Row, Col, List, Space, message, Statistic } from "antd";
import { CloseOutlined, CheckOutlined } from "@ant-design/icons";
import LayoutWrapper from "@iso/components/utility/layoutWrapper";
import DashboardStyleWrapper from "@iso/containers/BlankPage/BlankPage.styles";
import PageHeader from "@iso/components/utility/pageHeader";
import { useDispatch, useSelector } from "react-redux";
import orderActions from "@iso/redux/order/actions";
import { firebaseApp } from "@iso/config/firebase.config";
import productActions from "@iso/redux/product/actions";
import categoryActions from "@iso/redux/category/actions";
import trademarkActions from "@iso/redux/trademark/actions";
import userActions from "@iso/redux/user/actions";

const BlankPage = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [edit, setEdit] = useState(false);
  const [totalInvoices, setTotalInvoices] = useState(0);
  const {
    Product: { totalProduct },
    Category: { totalCategory },
    Trademark: { totalTrademark },
    User: { totalUser },
  } = useSelector((state) => state);
  const dispatch = useDispatch();

  useEffect(() => {
    Promise.all([
      dispatch(productActions.getProducts()),
      dispatch(categoryActions.getCategories()),
      dispatch(trademarkActions.getTrademarks()),
      dispatch(userActions.getUsers()),
    ]).then(() => {});
  }, [dispatch]);

  useEffect(() => {
    const List = [];
    firebaseApp
      .firestore()
      .collection("invoices")
      .where("status", "==", "Chờ xử lý")
      .get()
      .then((querySnapshot) => {
        setTotalInvoices(querySnapshot.size);
        querySnapshot.forEach((doc) => {
          const list = doc.data();
          List.push({
            id: doc.id,
            ...list,
          });
        });
      })
      .then(() => {
        setData(List);
        setLoading(false);
        setEdit(!edit);
      })
      .catch(() => setLoading(false));
  }, [edit]);

  // eslint-disable-next-line react/prop-types
  const IconText = ({ icon, text }) => (
    <Space>
      {React.createElement(icon)}
      {text}
    </Space>
  );

  const handleChange = useCallback(
    (status, orderId) => {
      new Promise((resolve, reject) => {
        return dispatch(
          orderActions.changeOrderStatus({ status, orderId, resolve, reject })
        );
      })
        .then(() => {
          message.success("Đơn đã được duyệt");
          setEdit(!edit);
        })
        .catch(() => {
          message.error("Lỗi");
        });
    },
    [edit, dispatch]
  );

  return (
    <LayoutWrapper>
      <PageHeader>Dashboard</PageHeader>
      <DashboardStyleWrapper>
        <Row>
          <Col xs={24} md={12}>
            <Row className="layout">
              <Col span={24}>
                <div style={{ alignItems: "center", display: "flex" }}>
                  <img
                    alt="IMG"
                    style={{ width: 80, height: 80 }}
                    src={require("@iso/assets/images/dashboard/aaaaaasdf.webp")}
                  />
                  <span style={{ fontSize: 20, fontWeight: "bold" }}>
                    NGƯỜI DÙNG
                  </span>
                </div>
              </Col>
              <Col span={12} style={{ paddingTop: 10 }}>
                <Statistic title="Tổng số người dùng" value={totalUser} />
              </Col>
              <Col span={12} style={{ paddingTop: 10 }}>
                <Statistic title="Người dùng đăng ký mới" value={3} />
              </Col>
            </Row>
            <Row className="layout" style={{ marginTop: 50 }}>
              <Col span={24}>
                <div style={{ alignItems: "center", display: "flex" }}>
                  <img
                    alt="IMG"
                    style={{ width: 80, height: 80 }}
                    src={require("@iso/assets/images/dashboard/product.png")}
                  />
                  <span style={{ fontSize: 20, fontWeight: "bold" }}>SẢN PHẨM</span>
                </div>
              </Col>
              <Col span={12} style={{ paddingTop: 10 }}>
                <Statistic title="Tổng số sản phẩm" value={totalProduct} />
              </Col>
              <Col span={12} style={{ paddingTop: 10 }}>
                <Statistic
                  title="Tổng giá trị (VNĐ)"
                  value={2433666222}
                  precision={2}
                />
              </Col>
            </Row>
            <Row className="layout" style={{ marginTop: 50 }}>
              <Col span={12} style={{ paddingTop: 10 }}>
                <Col span={24}>
                  <div style={{ alignItems: "center", display: "flex" }}>
                    <img
                      alt="IMG"
                      style={{ width: 80, height: 80 }}
                      src={require("@iso/assets/images/dashboard/ctg.png")}
                    />
                    <span style={{ fontSize: 20, fontWeight: "bold" }}>
                      DANH MỤC
                    </span>
                  </div>
                </Col>
                <Statistic title="Tổng số danh mục" value={totalCategory} />
              </Col>
              <Col span={12} style={{ paddingTop: 10 }}>
                <Col span={24}>
                  <div style={{ alignItems: "center", display: "flex" }}>
                    <img
                      alt="IMG"
                      style={{ width: 80, height: 80 }}
                      src={require("@iso/assets/images/dashboard/building.webp")}
                    />
                    <span style={{ fontSize: 20, fontWeight: "bold" }}>
                      HÃNG SẢN XUẤT
                    </span>
                  </div>
                </Col>
                <Statistic title="Tổng số hãng sản xuất" value={totalTrademark} />
              </Col>
            </Row>
          </Col>
          <Col xs={24} md={{ span: 11, offset: 1 }} className="layout">
            <Col span={24}>
              <div style={{ alignItems: "center", display: "flex" }}>
                <img
                  style={{ width: 80, height: 80 }}
                  src={require("@iso/assets/images/dashboard/INVOICE.png")}
                  alt="img"
                />
                <span style={{ fontSize: 20, fontWeight: "bold" }}>
                  ĐƠN CHỜ XỬ LÝ ({totalInvoices})
                </span>
              </div>
            </Col>
            <List
              itemLayout="vertical"
              size="large"
              loading={loading}
              pagination={{
                pageSize: 5,
              }}
              dataSource={data}
              renderItem={(item, index) => (
                <List.Item
                  key={index}
                  actions={[
                    <div
                      style={{ color: "green" }}
                      key="1"
                      onClick={() => handleChange("Đang xử lý", item.id)}
                    >
                      <IconText
                        icon={CheckOutlined}
                        text="Duyệt đơn"
                        key="list-vertical-star-o"
                      />
                    </div>,
                    <div
                      style={{ color: "red" }}
                      key="2"
                      onClick={() => handleChange("Đã hủy", item.id)}
                    >
                      <IconText
                        icon={CloseOutlined}
                        text="Hủy đơn"
                        key="list-vertical-like-o"
                      />
                    </div>,
                  ]}
                >
                  <List.Item.Meta
                    title={item.displayName}
                    description={`Email: ${item.email} -- Số điện thoại: ${item.phoneNumber}`}
                  />
                  <div>Địa chỉ: {item.address}</div>
                  <div>Ngày tạo: {item.createDate}</div>
                  <div>Giá trị đơn hàng: {item.totalMoney}</div>
                </List.Item>
              )}
            />
          </Col>
        </Row>
      </DashboardStyleWrapper>
    </LayoutWrapper>
  );
};

export default BlankPage;
