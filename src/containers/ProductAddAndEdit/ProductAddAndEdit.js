import React, { useState, useEffect, useCallback } from "react";
import { Row, Col, Steps, Button, Upload, notification, Modal, message } from "antd";
import { Formik, FieldArray } from "formik";
import { Form, Input, Select, InputNumber } from "formik-antd";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import { useParams } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import productActions from "@iso/redux/product/actions";
import { isEmpty } from "lodash";
import Loader from "@iso/components/utility/loader";
import {
  TRADEMARKS,
  CARD_DESIGN,
  MEMORY_CARD,
  GUARANTEE,
  HARD_DISK,
  OS,
  RAM,
} from "@iso/constants/common.constants";
import ImgCrop from "antd-img-crop";
import moment from "moment";

import LayoutWrapper from "@iso/components/utility/layoutWrapper";
import LayoutContent from "@iso/components/utility/layoutContent";
import PageHeader from "@iso/components/utility/pageHeader";
import ProductAddStyleWrapper from "@iso/containers/ProductAddAndEdit/ProductAddAndEdit.styles";
import validationSchema from "@iso/containers/ProductAddAndEdit/Validation.schema";
import { firebaseApp } from "@iso/config/firebase.config";

const { Step } = Steps;

const ProductAddAndEdit = () => {
  const { productId } = useParams();
  const dispatch = useDispatch();
  const { product, loading } = useSelector((state) => state.Product);
  const [current, setCurrent] = useState(0);
  const [initialValues, setInitialValues] = useState(initialValue);
  const [fileList, setFileList] = useState([]);
  const [check, setCheck] = useState("PRODUCT");
  const [modalVisible, setModalVisible] = useState(true);
  const [imageUrl, setImageUrl] = useState(null);
  const [uploading, setUploading] = useState(false);
  const [fileUploading, setFileUploading] = useState(false);
  const date = Date.now();

  useEffect(() => {
    if (productId) {
      dispatch(productActions.getProduct({ productId }));
    }
  }, [dispatch, productId]);

  useEffect(() => {
    if (productId && !isEmpty(product)) {
      setInitialValues(product);
      setImageUrl(product.productImage);
    }
  }, [product, productId]);

  const openNotification = (placement, message) => {
    notification.info({
      message: "Thông báo",
      description: message,
      placement,
    });
  };

  const next = () => {
    setCurrent(current + 1);
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  const handleSubmit = useCallback(
    (data, form) => {
      data.productImage = imageUrl;
      data.images = fileList;
      data.createDate = moment(date).format("DD/MM/YYYY HH:mm:ss");
      new Promise((resolve, reject) => {
        if (productId) {
          return dispatch(
            productActions.editProduct({ productId, data, resolve, reject })
          );
        } else {
          return dispatch(productActions.addProduct({ data, resolve, reject }));
        }
      })
        .then(() => {
          openNotification("topRight", "Thêm thành công");
          form.setSubmitting(false);
          setCurrent(0);
        })
        .catch((e) => {
          openNotification("topRight", `Lỗi: ${e.message}`);
          form.setSubmitting(false);
        });
    },
    [date, dispatch, fileList, imageUrl, productId]
  );

  const getBase64 = (img, callback) => {
    // eslint-disable-next-line no-undef
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  };

  const onFileChange = (info) => {
    if (info.file.status === "uploading") {
      setUploading(true);
      return;
    }
    if (info.file.status === "done") {
      getBase64(info.file.originFileObj, (imageUrl) => {
        setImageUrl(imageUrl);
        setUploading(false);
      });
    }
  };

  const beforeUpload = (file) => {
    const isImage = file.type.indexOf("image/") === 0;
    if (!isImage) {
      message.error("Bạn chỉ được tải lên file hình ảnh");
    }
    const isLt5M = file.size / 1024 / 1024 < 5;
    if (!isLt5M) {
      message.error("Hình ảnh phải nhỏ hơn 5mb");
    }
    return isImage && isLt5M;
  };

  const customUpload = async ({ onError, onSuccess, file }) => {
    const storage = firebaseApp.storage();
    const metadata = {
      contentType: "image/jpeg",
    };
    const storageRef = await storage.ref();
    const imgFile = storageRef.child(`images/${file.name}`);
    try {
      const image = await imgFile.put(file, metadata);
      firebaseApp
        .storage()
        .ref("images")
        .child(file.name)
        .getDownloadURL()
        .then((fireBaseUrl) => {
          setImageUrl(fireBaseUrl);
        });
      onSuccess(null, image);
    } catch (e) {
      onError(e);
    }
  };

  const customListUpload = async ({ onError, onSuccess, file }) => {
    const storage = firebaseApp.storage();
    const metadata = {
      contentType: "image/jpeg",
    };
    const storageRef = await storage.ref();
    const imgFile = storageRef.child(`images/${file.name}`);
    try {
      const image = await imgFile.put(file, metadata);
      firebaseApp
        .storage()
        .ref("images")
        .child(file.name)
        .getDownloadURL()
        .then((fireBaseUrl) => {
          setFileList([...fileList, fireBaseUrl]);
          setFileUploading(false);
        });
      onSuccess(null, image);
    } catch (e) {
      onError(e);
    }
  };

  const onChange = (info) => {
    if (info.file.status === "uploading") {
      setFileUploading(true);
      return;
    }
    if (info.file.status === "done") {
      getBase64(info.file.originFileObj, (imageUrl) => {
        setFileList([...fileList, imageUrl]);
      });
    }
  };

  const ModalVisible = (modal, check) => {
    setModalVisible(modal);
    setCheck(check);
  };

  return (
    <LayoutWrapper>
      <PageHeader>{productId ? "Sửa sản phẩm" : "Thêm sản phẩm"}</PageHeader>
      <LayoutContent>
        <Modal
          title="Bạn muốn thêm"
          centered
          visible={modalVisible}
          onOk={() => ModalVisible(false, "PRODUCT")}
          onCancel={() => ModalVisible(false, "ACCESSORIES")}
          okText="Sản phẩm"
          cancelText="Phụ kiện"
        >
          <div>
            <p>Chọn loại sản phẩm bạn muốn thêm</p>
          </div>
        </Modal>
        <ProductAddStyleWrapper>
          {loading ? (
            <div style={{ height: "100vh" }}>
              <Loader />
            </div>
          ) : (
            <Row>
              <Formik
                validationSchema={validationSchema()}
                initialValues={initialValues}
                enableReinitialize={true}
                onSubmit={handleSubmit}
                render={({ isSubmitting, values, dirty, isValid }) => (
                  <Form
                    colon={false}
                    justify="center"
                    className="w-100"
                    {...layout}
                    labelAlign="left"
                  >
                    <Steps current={current}>
                      <Step key={1} title="Thông tin cơ bản" />
                      <Step key={2} title="Thông tin cấu hình" />
                      <Step key={3} title="Hình ảnh" />
                    </Steps>
                    <Row className="steps-content">
                      {current === 0 ? (
                        <Col xs={24} md={12}>
                          <Form.Item name="name" required label="Tên sản phẩm">
                            <Input name="name" />
                          </Form.Item>
                          <Form.Item name="oldPrice" label="Giá gốc">
                            <Input name="oldPrice" />
                          </Form.Item>
                          <Form.Item name="price" required label="Giá sản phẩm">
                            <Input name="price" />
                          </Form.Item>
                          {check === "PRODUCT" && (
                            <Form.Item
                              name="trademark"
                              required
                              label="Hãng sản xuất"
                            >
                              <Select name="trademark">
                                {TRADEMARKS.map((select, index) => (
                                  <Select.Option key={index} value={select}>
                                    {select}
                                  </Select.Option>
                                ))}
                              </Select>
                            </Form.Item>
                          )}
                          <Form.Item name="categories" required label="Phân loại">
                            <FieldArray
                              name="categories"
                              render={(arrayHelpers) => (
                                <div>
                                  {values.categories &&
                                    values.categories.length &&
                                    values.categories.map((category, index) => (
                                      <div key={index}>
                                        <Row>
                                          <Col lg={18} xs={24}>
                                            <Form.Item name={`categories.${index}`}>
                                              <Input
                                                placeholder="Business, Macbook, Gaming, Graphic"
                                                name={`categories.${index}`}
                                              />
                                            </Form.Item>
                                          </Col>
                                          <Col lg={6} xs={8}>
                                            <Button
                                              type="dashed"
                                              disabled={
                                                values.categories.length <= 1
                                              }
                                              style={{
                                                marginRight: 4,
                                                marginLeft: 4,
                                              }}
                                              onClick={() =>
                                                arrayHelpers.remove(index)
                                              }
                                            >
                                              -
                                            </Button>
                                            <Button
                                              type="dashed"
                                              disabled={
                                                check === "ACCESSORIES" ||
                                                values.images.length >= 3
                                              }
                                              onClick={() => arrayHelpers.push("")}
                                            >
                                              +
                                            </Button>
                                          </Col>
                                        </Row>
                                      </div>
                                    ))}
                                </div>
                              )}
                            />
                          </Form.Item>
                          <Form.Item name="guarantee" required label="Bảo hành">
                            <Select name="guarantee">
                              {GUARANTEE.map((select, index) => (
                                <Select.Option key={index} value={select}>
                                  {select}
                                </Select.Option>
                              ))}
                            </Select>
                          </Form.Item>
                          <Form.Item name="amount" required label="Số lượng">
                            <InputNumber
                              style={{ width: "100%" }}
                              min={1}
                              defaultValue={1}
                              name="amount"
                            />
                          </Form.Item>
                          <Form.Item name="productImage" label="Hình sản phẩm">
                            <ImgCrop rotate>
                              <Upload
                                name="productImage"
                                listType="picture-card"
                                className="avatar-uploader"
                                showUploadList={false}
                                beforeUpload={beforeUpload}
                                onChange={onFileChange}
                                customRequest={customUpload}
                              >
                                {imageUrl ? (
                                  <img
                                    src={imageUrl}
                                    alt="productImage"
                                    style={{ width: "100%" }}
                                  />
                                ) : (
                                  <div>
                                    {uploading ? (
                                      <LoadingOutlined />
                                    ) : (
                                      <PlusOutlined />
                                    )}
                                    <div className="ant-upload-text">Tải lên</div>
                                  </div>
                                )}
                              </Upload>
                            </ImgCrop>
                          </Form.Item>
                        </Col>
                      ) : current === 1 ? (
                        <Row className="w-100 m-10">
                          {check === "PRODUCT" && (
                            <>
                              <Col xs={24} sm={11}>
                                <Form.Item name="cpuType" label="Loại CPU">
                                  <Input name="cpuType" />
                                </Form.Item>
                                <Form.Item name="ramMemory" label="Bộ nhớ RAM">
                                  <Select name="ramMemory">
                                    {RAM.map((select, index) => (
                                      <Select.Option key={index} value={select}>
                                        {select}
                                      </Select.Option>
                                    ))}
                                  </Select>
                                </Form.Item>
                                <Form.Item name="hardDisk" label="Ổ cứng">
                                  <Select name="hardDisk">
                                    {HARD_DISK.map((select, index) => (
                                      <Select.Option key={index} value={select}>
                                        {select}
                                      </Select.Option>
                                    ))}
                                  </Select>
                                </Form.Item>
                                <Form.Item
                                  name="screenSize"
                                  label="Kích thước màn hình"
                                >
                                  <Input name="screenSize" />
                                </Form.Item>
                                <Form.Item
                                  name="screenTechnology"
                                  label="Công nghệ màn hình"
                                >
                                  <Input name="screenTechnology" />
                                </Form.Item>
                                <Form.Item name="memoryCard" label="Bộ nhớ Card">
                                  <Select name="memoryCard">
                                    {MEMORY_CARD.map((select, index) => (
                                      <Select.Option key={index} value={select}>
                                        {select}
                                      </Select.Option>
                                    ))}
                                  </Select>
                                </Form.Item>
                                <Form.Item name="pinInfo" label="Thông tin Pin">
                                  <Input name="pinInfo" />
                                </Form.Item>
                                <Form.Item name="port" label="Cổng kết nối">
                                  <Input name="port" />
                                </Form.Item>
                                <Form.Item name="weight" label="Trọng lượng">
                                  <Input name="weight" />
                                </Form.Item>
                              </Col>
                              <Col xs={24} sm={11}>
                                <Form.Item name="cpuInfo" label="Thông tin CPU">
                                  <Input name="cpuInfo" />
                                </Form.Item>
                                <Form.Item name="ramInfo" label="Thông tin RAM">
                                  <Input name="ramInfo" />
                                </Form.Item>
                                <Form.Item
                                  name="hardDiskInfo"
                                  label="Thông tin ổ cứng"
                                >
                                  <Input name="hardDiskInfo" />
                                </Form.Item>
                                <Form.Item
                                  name="resolution"
                                  label="Độ phân giải (W x H)"
                                >
                                  <Input name="resolution" />
                                </Form.Item>
                                <Form.Item name="graphicCard" label="Card màn hình">
                                  <Input name="graphicCard" />
                                </Form.Item>
                                <Form.Item name="cardDesign" label="Loại card">
                                  <Select name="cardDesign">
                                    {CARD_DESIGN.map((select, index) => (
                                      <Select.Option key={index} value={select}>
                                        {select}
                                      </Select.Option>
                                    ))}
                                  </Select>
                                </Form.Item>
                                <Form.Item
                                  name="audioTechnology"
                                  label="Công nghệ âm thanh"
                                >
                                  <Input name="audioTechnology" />
                                </Form.Item>
                                <Form.Item name="size" label="Kích thước">
                                  <Input name="size" />
                                </Form.Item>
                                <Form.Item
                                  name="operatingSystem"
                                  label="Hệ điều hành"
                                >
                                  <Select name="operatingSystem">
                                    {OS.map((select, index) => (
                                      <Select.Option key={index} value={select}>
                                        {select}
                                      </Select.Option>
                                    ))}
                                  </Select>
                                </Form.Item>
                              </Col>
                            </>
                          )}
                          <Col xs={24} sm={11}>
                            <Form.Item required name="description" label="Mô tả">
                              <Input name="description" />
                            </Form.Item>
                          </Col>
                          <Col xs={24} sm={11}>
                            <Form.Item name="note" label="Ghi chú">
                              <Input name="note" />
                            </Form.Item>
                          </Col>
                        </Row>
                      ) : (
                        <Row className="w-100 m-10">
                          <Col xs={24} md={7}>
                            <Form.Item name="images" label="Hình ảnh">
                              <ImgCrop rotate>
                                <Upload
                                  name="images"
                                  listType="picture-card"
                                  className="avatar-uploader"
                                  fileList={fileList}
                                  showUploadList={false}
                                  beforeUpload={beforeUpload}
                                  onChange={onChange}
                                  customRequest={customListUpload}
                                >
                                  {fileList.length < 5 && (
                                    <div>
                                      {fileUploading ? (
                                        <LoadingOutlined />
                                      ) : (
                                        <PlusOutlined />
                                      )}
                                      <div className="ant-upload-text">Tải lên</div>
                                    </div>
                                  )}
                                </Upload>
                              </ImgCrop>
                            </Form.Item>
                          </Col>
                          <Col xs={24} md={15}>
                            <Row>
                              {fileList &&
                                fileList.map((url, index) => (
                                  <Col
                                    xs={12}
                                    md={{ span: 3, offset: 1 }}
                                    key={index}
                                  >
                                    <img
                                      src={url}
                                      alt="images"
                                      style={{ width: "100%" }}
                                    />
                                  </Col>
                                ))}
                            </Row>
                          </Col>
                        </Row>
                      )}
                    </Row>
                    <Row justify="end">
                      <div className="steps-action">
                        {current > 0 && (
                          <Button style={{ margin: "0 8px" }} onClick={() => prev()}>
                            Quay lại
                          </Button>
                        )}
                        {(current === 0 || current === 1) && (
                          <Button type="primary" onClick={() => next()}>
                            Tiếp
                          </Button>
                        )}
                        {current === 2 && (
                          <Button
                            type="primary"
                            htmlType="submit"
                            disabled={!(dirty && isValid)}
                            loading={isSubmitting}
                          >
                            Lưu
                          </Button>
                        )}
                      </div>
                    </Row>
                  </Form>
                )}
              />
            </Row>
          )}
        </ProductAddStyleWrapper>
      </LayoutContent>
    </LayoutWrapper>
  );
};

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

const initialValue = {
  categories: [""],
  name: null,
  oldPrice: null,
  price: null,
  guarantee: "6 Month",
  amount: 1,
  productImage: null,
  description: null,
  note: null,
  images: [""],
};

export default ProductAddAndEdit;
