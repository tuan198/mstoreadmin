import Yup from "@iso/config/validation.config";

const validationSchema = () =>
  Yup.object().shape({
    name: Yup.string().required().nullable().label("Tên sản phẩm"),
    description: Yup.string().required().nullable().label("Mô tả"),
    categories: Yup.array().required().nullable().label("Danh mục"),
    price: Yup.string().required().nullable().label("Giá bán"),
    guarantee: Yup.string().required().nullable().label("Bảo hành"),
    amount: Yup.number().min(1).required().nullable().label("Số lượng"),
  });

export default validationSchema;
