import styled from "styled-components";

const ProductAddStyleWrapper = styled.div`
  width: 100%;

  .steps-content {
    margin-top: 16px;
    border: 1px dashed #e9e9e9;
    border-radius: 2px;
    background-color: #fff;
    text-align: center;
    padding: 30px 0;
    justify-content: center;
    align-items: center;
  }

  .steps-action {
    margin-top: 24px;
  }

  .w-100 {
    width: 100%;
  }

  .m-10 {
    margin: 0px 50px;
    justify-content: space-between;
  }

  .ant-select {
    text-align: left;
  }

  .ant-form-item-label > label.ant-form-item-required::after {
    display: inline-block;
    margin-right: 4px;
    color: #ff4d4f;
    font-size: 14px;
    font-family: SimSun, sans-serif;
    line-height: 1;
    content: "*";
  }

  .ant-form-item-label > label.ant-form-item-required::before {
    margin-right: 0px;
    content: "";
  }
`;

export default ProductAddStyleWrapper;
