import React from "react";
import { Link } from "react-router-dom";
import siteConfig from "@iso/config/site.config";

import logo from "@iso/assets/images/icon/icon256.png";

export default ({ collapsed }) => {
  return (
    <div className="isoLogoWrapper">
      {collapsed ? (
        <div>
          <h3>
            <Link to="/dashboard">
              <img src={logo} width={40} alt="logo" />
            </Link>
          </h3>
        </div>
      ) : (
        <h3>
          <Link to="/dashboard">{siteConfig.siteName}</Link>
        </h3>
      )}
    </div>
  );
};
