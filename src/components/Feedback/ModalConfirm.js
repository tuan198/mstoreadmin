import React from "react";
import PropTypes from "prop-types";
import { Modal } from "antd";

const ModalConfirm = ({ children, title, content, okText, cancelText, onOk }) => {
    const confirm = () => {
        Modal.confirm({
            title: title || "Đăng xuất",
            content: content || "",
            okText: okText || "Có",
            cancelText: cancelText || "Không",
            onOk() {
                setTimeout(onOk, 200);
            },
        });
    };

    return <span onClick={confirm}>{children}</span>;
};

ModalConfirm.propTypes = {
    children: PropTypes.node,
    title: PropTypes.string,
    content: PropTypes.string,
    okText: PropTypes.string,
    cancelText: PropTypes.string,
    onOk: PropTypes.func,
};

export default ModalConfirm;
