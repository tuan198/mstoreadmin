import * as firebase from "firebase";
import "@firebase/firestore"; // 👈 If you're using firestore
import ReduxSagaFirebase from "redux-saga-firebase";

const myFirebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyB_mK_nK_RlKBtE2I6uBeorLqIRMVCwWbY",
  authDomain: "mstore-t8398.firebaseapp.com",
  databaseURL: "https://mstore-t8398.firebaseio.com",
  projectId: "mstore-t8398",
  storageBucket: "mstore-t8398.appspot.com",
  messagingSenderId: "808066891113",
  appId: "1:808066891113:web:a6b98720ec70c7c28351f8",
  measurementId: "G-H9NS5D8LLR",
});

export const firebaseApp = myFirebaseApp;

const rsf = new ReduxSagaFirebase(myFirebaseApp);
export default rsf;
