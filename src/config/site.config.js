export default {
  siteName: "MSTORE",
  siteIcon: "ion-flash",
  footerText: `Mstore @ ${new Date().getFullYear()} Created by Nguyen Tuan, RJC`,
  enableAnimatedRoute: false,
  apiUrl: "http://yoursite.com/api/",
  google: {
    analyticsKey: "UA-xxxxxxxxx-1",
  },
  dashboard: "/dashboard",
};
