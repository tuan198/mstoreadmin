export const ROLES = {
  ADMIN: "ADMIN",
  USER: "USER",
};

export const GENDER = ["Không xác định", "Nam", "Nữ"];
export const ROLE = [ROLES.ADMIN, ROLES.USER];

export const TRADEMARKS = [
  "Apple",
  "Dell",
  "Asus",
  "ThinkPad",
  "Razer",
  "MSI",
  "Acer",
  "HP",
  "Microsoft",
];
export const GUARANTEE = [
  "1 Month",
  "2 Month",
  "3 Month",
  "6 Month",
  "12 Month",
  "24 Month",
  "3 Year",
  "5 Year",
];
export const CATEGORIES = [
  { name: "Business" },
  { name: "Gaming" },
  { name: "Graphic" },
  { name: "Macbook" },
  { name: "Accessories" },
];
export const RAM = ["4 GB", "8 GB", "16 GB", "32 GB", "64 GB"];
export const HARD_DISK = [
  "120 GB",
  "128 GB",
  "240 GB",
  "250 GB",
  "256 GB",
  "512 GB",
  "1 TB",
  "2 TB",
  "4 TB",
  "8 TB",
];
export const MEMORY_CARD = [
  "256 MB",
  "512 MB",
  "1 GB",
  "1.5 GB",
  "2 GB",
  "4 GB",
  "6 GB",
  "8 GB",
  "10 GB",
];
export const CARD_DESIGN = ["VGA", "Onboard"];
export const OS = [
  "Window Xp",
  "Window 7",
  "Window 8",
  "Window 10",
  "MacOS",
  "Linux",
  "Chrome OS",
];
