import { all, takeEvery, put, fork, call } from "redux-saga/effects";
import actions from "@iso/redux/category/actions";
import rsf from "@iso/config/firebase.config";

export function* getCategoriesRequest() {
  yield takeEvery(actions.GET_CATEGORIES_REQUEST, function* () {
    try {
      const snapshot = yield call(rsf.firestore.getCollection, "categories");
      let categories = [];
      snapshot.forEach((category) => {
        categories.push({
          ...category.data(),
          id: category.id,
        });
      });
      yield put({
        type: actions.GET_CATEGORIES_SUCCESS,
        categories: categories ? categories : [],
        totalCategory: snapshot.size,
      });
    } catch (e) {
      yield put({ type: actions.GET_CATEGORIES_ERROR, error: e.message });
    }
  });
}

export function* getCategoryRequest() {
  yield takeEvery(actions.GET_CATEGORY_REQUEST, function* ({ payload }) {
    const { categoryId } = payload;
    try {
      const snapshot = yield call(
        rsf.firestore.getDocument,
        `categories/${categoryId}`
      );
      const category = snapshot.data();
      yield put({
        type: actions.GET_CATEGORY_SUCCESS,
        category: category ? category : {},
      });
    } catch (e) {
      yield put({ type: actions.GET_CATEGORY_ERROR, error: e.message });
    }
  });
}

export function* addCategoryRequest() {
  yield takeEvery(actions.ADD_CATEGORY_REQUEST, function* ({ payload }) {
    const { data, resolve, reject } = payload;
    const id = data.name;
    try {
      yield call(rsf.firestore.setDocument, `categories/${id}`, {
        ...data,
      });
      yield put({ type: actions.ADD_CATEGORY_SUCCESS });
      yield resolve();
    } catch (e) {
      yield put({ type: actions.ADD_CATEGORY_ERROR, error: e.message });
      reject(e);
    }
  });
}

export function* editCategoryRequest() {
  yield takeEvery(actions.EDIT_CATEGORY_REQUEST, function* ({ payload }) {
    const { categoryId, data, resolve, reject } = payload;
    try {
      yield call(rsf.firestore.setDocument, `categories/${categoryId}`, {
        ...data,
      });
      yield put({
        type: actions.EDIT_CATEGORY_SUCCESS,
        error: "Chỉnh sửa thông tin thành công",
      });
      yield resolve();
    } catch (e) {
      yield put({ type: actions.EDIT_CATEGORY_ERROR, error: e.message });
      reject(e);
    }
  });
}

export function* deleteCategoryRequest() {
  yield takeEvery(actions.DELETE_CATEGORY_REQUEST, function* ({ payload }) {
    const { categoryId, resolve, reject } = payload;
    try {
      yield call(rsf.firestore.deleteDocument, `categories/${categoryId}`);
      yield put({ type: actions.GET_CATEGORIES_REQUEST });
      yield resolve();
    } catch (e) {
      yield put({ type: actions.DELETE_CATEGORY_ERROR, error: e.message });
      reject(e);
    }
  });
}

export default function* rootSaga() {
  yield all([
    fork(getCategoriesRequest),
    fork(getCategoryRequest),
    fork(addCategoryRequest),
    fork(editCategoryRequest),
    fork(deleteCategoryRequest),
  ]);
}
