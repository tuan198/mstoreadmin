import actions from "@iso/redux/category/actions";

const initialState = {
  categories: [],
  category: {},
  loading: false,
  error: null,
  totalCategory: 0,
};

export default function categoryReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_CATEGORIES_REQUEST:
      return {
        ...state,
        loading: true,
        categories: [],
        error: null,
      };
    case actions.GET_CATEGORIES_SUCCESS:
      return {
        ...state,
        loading: false,
        categories: action.categories,
        error: null,
        totalCategory: action.totalCategory,
      };
    case actions.GET_CATEGORIES_ERROR:
      return {
        ...state,
        loading: false,
        categories: [],
        error: action.error,
      };
    case actions.GET_CATEGORY_REQUEST:
      return {
        ...state,
        loading: true,
        category: {},
        error: null,
      };
    case actions.GET_CATEGORY_SUCCESS:
      return {
        ...state,
        loading: false,
        category: action.category,
        error: null,
      };
    case actions.GET_CATEGORY_ERROR:
      return {
        ...state,
        loading: false,
        category: {},
        error: action.error,
      };
    case actions.ADD_CATEGORY_REQUEST:
      return {
        ...state,
        loading: true,
        category: {},
        error: null,
      };
    case actions.ADD_CATEGORY_SUCCESS:
      return {
        ...state,
        loading: false,
        category: action.category,
        error: null,
      };
    case actions.ADD_CATEGORY_ERROR:
      return {
        ...state,
        loading: false,
        category: {},
        error: action.error,
      };
    case actions.EDIT_CATEGORY_REQUEST:
      return {
        ...state,
        loading: true,
        category: {},
        error: null,
      };
    case actions.EDIT_CATEGORY_SUCCESS:
      return {
        ...state,
        loading: false,
        category: action.category,
        error: null,
      };
    case actions.EDIT_CATEGORY_ERROR:
      return {
        ...state,
        loading: false,
        category: {},
        error: action.error,
      };
    case actions.DELETE_CATEGORY_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case actions.DELETE_CATEGORY_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
      };
    case actions.DELETE_CATEGORY_ERROR:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}
