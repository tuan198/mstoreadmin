import { all, takeEvery, put, fork, call } from "redux-saga/effects";
import actions from "@iso/redux/product/actions";
import rsf from "@iso/config/firebase.config";

export function* getProductsRequest() {
  yield takeEvery(actions.GET_PRODUCTS_REQUEST, function* () {
    try {
      const snapshot = yield call(rsf.firestore.getCollection, "products");
      let products = [];
      snapshot.forEach((product) => {
        products.push({
          ...product.data(),
          id: product.id,
        });
      });
      yield put({
        type: actions.GET_PRODUCTS_SUCCESS,
        products: products,
        totalProduct: snapshot.size,
      });
    } catch (e) {
      yield put({ type: actions.GET_PRODUCTS_ERROR, error: e.message });
    }
  });
}

export function* getProductRequest() {
  yield takeEvery(actions.GET_PRODUCT_REQUEST, function* ({ payload }) {
    const { productId } = payload;
    try {
      const snapshot = yield call(
        rsf.firestore.getDocument,
        `products/${productId}`
      );
      const product = snapshot.data();
      yield put({ type: actions.GET_PRODUCT_SUCCESS, product: product });
    } catch (e) {
      yield put({ type: actions.GET_PRODUCT_ERROR, error: e.message });
    }
  });
}

export function* addProductRequest() {
  yield takeEvery(actions.ADD_PRODUCT_REQUEST, function* ({ payload }) {
    const { data, resolve, reject } = payload;
    try {
      yield call(rsf.firestore.addDocument, "products", {
        ...data,
      });
      yield put({ type: actions.ADD_PRODUCT_SUCCESS });
      yield resolve();
    } catch (e) {
      yield put({ type: actions.ADD_PRODUCT_ERROR, error: e.message });
      reject(e);
    }
  });
}

export function* editProductRequest() {
  yield takeEvery(actions.EDIT_PRODUCT_REQUEST, function* ({ payload }) {
    const { productId, data, resolve, reject } = payload;
    try {
      yield call(rsf.firestore.setDocument, `products/${productId}`, {
        ...data,
      });
      yield put({
        type: actions.EDIT_PRODUCT_SUCCESS,
        error: "Chỉnh sửa thông tin thành công",
      });
      yield resolve();
    } catch (e) {
      yield put({ type: actions.EDIT_PRODUCT_ERROR, error: e.message });
      reject(e);
    }
  });
}

export default function* rootSaga() {
  yield all([
    fork(getProductsRequest),
    fork(getProductRequest),
    fork(addProductRequest),
    fork(editProductRequest),
  ]);
}
