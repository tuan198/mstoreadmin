import actions from "@iso/redux/product/actions";

const initialState = {
  products: [],
  product: [],
  loading: false,
  error: null,
  totalProduct: 0,
};

export default function productReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_PRODUCTS_REQUEST:
      return {
        ...state,
        loading: true,
        products: [],
        error: null,
      };
    case actions.GET_PRODUCTS_SUCCESS:
      return {
        ...state,
        loading: false,
        products: action.products,
        error: null,
        totalProduct: action.totalProduct,
      };
    case actions.GET_PRODUCTS_ERROR:
      return {
        ...state,
        loading: false,
        products: [],
        error: action.error,
      };
    case actions.GET_PRODUCT_REQUEST:
      return {
        ...state,
        loading: true,
        products: [],
        error: null,
      };
    case actions.GET_PRODUCT_SUCCESS:
      return {
        ...state,
        loading: false,
        product: action.product,
        error: null,
      };
    case actions.GET_PRODUCT_ERROR:
      return {
        ...state,
        loading: false,
        product: [],
        error: action.error,
      };
    case actions.ADD_PRODUCT_REQUEST:
      return {
        ...state,
        loading: true,
        products: [],
        error: null,
      };
    case actions.ADD_PRODUCT_SUCCESS:
      return {
        ...state,
        loading: false,
        product: action.product,
        error: null,
      };
    case actions.ADD_PRODUCT_ERROR:
      return {
        ...state,
        loading: false,
        product: [],
        error: action.error,
      };
    case actions.EDIT_PRODUCT_REQUEST:
      return {
        ...state,
        loading: true,
        products: [],
        error: null,
      };
    case actions.EDIT_PRODUCT_SUCCESS:
      return {
        ...state,
        loading: false,
        product: action.product,
        error: null,
      };
    case actions.EDIT_PRODUCT_ERROR:
      return {
        ...state,
        loading: false,
        product: [],
        error: action.error,
      };
    default:
      return state;
  }
}
