const actions = {
  GET_PRODUCTS_REQUEST: "GET_PRODUCTS_REQUEST",
  GET_PRODUCTS_SUCCESS: "GET_PRODUCTS_SUCCESS",
  GET_PRODUCTS_ERROR: "GET_PRODUCTS_ERROR",

  GET_PRODUCT_REQUEST: "GET_PRODUCT_REQUEST",
  GET_PRODUCT_SUCCESS: "GET_PRODUCT_SUCCESS",
  GET_PRODUCT_ERROR: "GET_PRODUCT_ERROR",

  ADD_PRODUCT_REQUEST: "ADD_PRODUCT_REQUEST",
  ADD_PRODUCT_SUCCESS: "ADD_PRODUCT_SUCCESS",
  ADD_PRODUCT_ERROR: "ADD_PRODUCT_ERROR",

  EDIT_PRODUCT_REQUEST: "EDIT_PRODUCT_REQUEST",
  EDIT_PRODUCT_SUCCESS: "EDIT_PRODUCT_SUCCESS",
  EDIT_PRODUCT_ERROR: "EDIT_PRODUCT_ERROR",

  getProducts: () => ({
    type: actions.GET_PRODUCTS_REQUEST,
  }),

  getProduct: (payload) => ({
    type: actions.GET_PRODUCT_REQUEST,
    payload: payload,
  }),

  addProduct: (payload) => ({
    type: actions.ADD_PRODUCT_REQUEST,
    payload: payload,
  }),

  editProduct: (payload) => ({
    type: actions.EDIT_PRODUCT_REQUEST,
    payload: payload,
  }),
};

export default actions;
