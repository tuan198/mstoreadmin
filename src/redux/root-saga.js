import { all } from "redux-saga/effects";
import authSagas from "@iso/redux/auth/saga";
import profileSaga from "@iso/redux/profile/saga";
import productSagas from "@iso/redux/product/saga";
import categorySagas from "@iso/redux/category/saga";
import trademarkSagas from "@iso/redux/trademark/saga";
import userSagas from "@iso/redux/user/saga";
import orderSagas from "@iso/redux/order/saga";

export default function* rootSaga() {
  yield all([
    authSagas(),
    profileSaga(),
    productSagas(),
    categorySagas(),
    trademarkSagas(),
    userSagas(),
    orderSagas(),
  ]);
}
