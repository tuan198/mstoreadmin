import actions from "./actions";
import { getAccessToken } from "@iso/lib/helpers/utility";

const initState = {
  userData: [],
  idToken: getAccessToken(),
  loginChecking: false,
  error: null,
  errorCheckAuthen: null,
  checkAuth: false,
};

export default function authReducer(state = initState, action) {
  switch (action.type) {
    case actions.CHECK_AUTHORIZATION:
      return {
        ...state,
        userData: [],
        errorCheckAuthen: null,
        checkAuth: true,
      };
    case actions.CHECK_AUTHORIZATION_ERROR:
      return {
        ...state,
        errorCheckAuthen: action.error,
      };
    case actions.LOGIN_REQUEST:
      return {
        ...state,
        error: null,
      };
    case actions.LOGIN_SUCCESS:
      return {
        ...state,
        error: null,
        idToken: action.idToken,
        userData: action.userData,
      };
    case actions.LOGIN_ERROR:
      return {
        ...state,
        error: action.error,
      };
    case actions.LOGOUT:
      return initState;
    default:
      return state;
  }
}
