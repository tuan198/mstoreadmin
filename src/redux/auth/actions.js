const actions = {
  CHECK_AUTHORIZATION: "CHECK_AUTHORIZATION",
  CHECK_AUTHORIZATION_ERROR: "CHECK_AUTHORIZATION_ERROR",
  LOGIN_REQUEST: "LOGIN_REQUEST",
  LOGOUT: "LOGOUT",
  LOGIN_SUCCESS: "LOGIN_SUCCESS",
  LOGIN_ERROR: "LOGIN_ERROR",
  RESET_AUTH: "RESET_AUTH",
  checkAuthorization: (payload) => ({
    type: actions.CHECK_AUTHORIZATION,
    payload,
  }),
  login: (payload) => ({
    type: actions.LOGIN_REQUEST,
    payload: payload,
  }),
  logout: () => ({
    type: actions.LOGOUT,
  }),
};
export default actions;
