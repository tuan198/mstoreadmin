import { all, takeEvery, put, fork, call, take } from "redux-saga/effects";
import { createBrowserHistory } from "history";
import rsf from "@iso/config/firebase.config";
import { clearToken, getAccessToken, setToken } from "@iso/lib/helpers/utility";
import actions from "./actions";

const history = createBrowserHistory();

export function* loginRequest() {
  yield takeEvery("LOGIN_REQUEST", function* ({ payload }) {
    const { email, password } = payload;
    try {
      const data = yield call(rsf.auth.signInWithEmailAndPassword, email, password);
      yield setToken(data.user.uid);
      yield put({
        type: actions.LOGIN_SUCCESS,
        userData: data.user,
        idToken: data.user.uid,
      });
    } catch (e) {
      yield put({ type: actions.LOGIN_ERROR, error: e.message });
    }
  });
}

export function* logout() {
  yield takeEvery(actions.LOGOUT, function* () {
    yield clearToken();
    yield call(rsf.auth.signOut);
    history.push("/");
  });
}

export function* checkAuthorization() {
  yield takeEvery(actions.CHECK_AUTHORIZATION, function* ({ payload }) {
    const { resolve, reject } = payload;
    const currentUser = yield call(rsf.auth.channel);
    while (true) {
      const { error, user } = yield take(currentUser);
      if (user) {
        yield put({
          type: actions.LOGIN_SUCCESS,
          idToken: getAccessToken(),
          userData: user,
        });
        yield resolve(currentUser);
      } else {
        yield reject();
        yield put({ type: actions.LOGIN_ERROR, error: error });
      }
    }
  });
}

// export function* checkRole() {
//
// }

export default function* rootSaga() {
  yield all([fork(checkAuthorization), fork(loginRequest), fork(logout)]);
}
