import actions from "@iso/redux/trademark/actions";

const initialState = {
  trademarks: [],
  trademark: {},
  loading: false,
  error: null,
  totalTrademark: 0,
};

export default function trademarkReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_TRADEMARKS_REQUEST:
      return {
        ...state,
        loading: true,
        trademarks: [],
        error: null,
      };
    case actions.GET_TRADEMARKS_SUCCESS:
      return {
        ...state,
        loading: false,
        trademarks: action.trademarks,
        error: null,
        totalTrademark: action.totalTrademark,
      };
    case actions.GET_TRADEMARKS_ERROR:
      return {
        ...state,
        loading: false,
        trademarks: [],
        error: action.error,
      };
    case actions.GET_TRADEMARK_REQUEST:
      return {
        ...state,
        loading: true,
        trademark: {},
        error: null,
      };
    case actions.GET_TRADEMARK_SUCCESS:
      return {
        ...state,
        loading: false,
        trademark: action.trademark,
        error: null,
      };
    case actions.GET_TRADEMARK_ERROR:
      return {
        ...state,
        loading: false,
        trademark: {},
        error: action.error,
      };
    case actions.ADD_TRADEMARK_REQUEST:
      return {
        ...state,
        loading: true,
        trademark: {},
        error: null,
      };
    case actions.ADD_TRADEMARK_SUCCESS:
      return {
        ...state,
        loading: false,
        trademark: action.trademark,
        error: null,
      };
    case actions.ADD_TRADEMARK_ERROR:
      return {
        ...state,
        loading: false,
        trademark: {},
        error: action.error,
      };
    case actions.EDIT_TRADEMARK_REQUEST:
      return {
        ...state,
        loading: true,
        trademark: {},
        error: null,
      };
    case actions.EDIT_TRADEMARK_SUCCESS:
      return {
        ...state,
        loading: false,
        trademark: action.trademark,
        error: null,
      };
    case actions.EDIT_TRADEMARK_ERROR:
      return {
        ...state,
        loading: false,
        trademark: {},
        error: action.error,
      };
    case actions.DELETE_TRADEMARK_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case actions.DELETE_TRADEMARK_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
      };
    case actions.DELETE_TRADEMARK_ERROR:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}
