import { all, takeEvery, put, fork, call } from "redux-saga/effects";
import actions from "@iso/redux/trademark/actions";
import rsf from "@iso/config/firebase.config";

export function* getTrademarksRequest() {
  yield takeEvery(actions.GET_TRADEMARKS_REQUEST, function* () {
    try {
      const snapshot = yield call(rsf.firestore.getCollection, "trademarks");
      let trademarks = [];
      snapshot.forEach((trademark) => {
        trademarks.push({
          ...trademark.data(),
          id: trademark.id,
        });
      });
      yield put({
        type: actions.GET_TRADEMARKS_SUCCESS,
        trademarks: trademarks ? trademarks : [],
        totalTrademark: snapshot.size,
      });
    } catch (e) {
      yield put({ type: actions.GET_TRADEMARKS_ERROR, error: e.message });
    }
  });
}

export function* getTrademarkRequest() {
  yield takeEvery(actions.GET_TRADEMARK_REQUEST, function* ({ payload }) {
    const { trademarkId } = payload;
    try {
      const snapshot = yield call(
        rsf.firestore.getDocument,
        `trademarks/${trademarkId}`
      );
      const trademark = snapshot.data();
      yield put({
        type: actions.GET_TRADEMARK_SUCCESS,
        trademark: trademark ? trademark : {},
      });
    } catch (e) {
      yield put({ type: actions.GET_TRADEMARK_ERROR, error: e.message });
    }
  });
}

export function* addTrademarkRequest() {
  yield takeEvery(actions.ADD_TRADEMARK_REQUEST, function* ({ payload }) {
    const { data, resolve, reject } = payload;
    const id = data.name;
    try {
      yield call(rsf.firestore.setDocument, `trademarks/${id}`, {
        ...data,
      });
      yield put({ type: actions.ADD_TRADEMARK_SUCCESS });
      yield resolve();
    } catch (e) {
      yield put({ type: actions.ADD_TRADEMARK_ERROR, error: e.message });
      reject(e);
    }
  });
}

export function* editTrademarkRequest() {
  yield takeEvery(actions.EDIT_TRADEMARK_REQUEST, function* ({ payload }) {
    const { trademarkId, data, resolve, reject } = payload;
    try {
      yield call(rsf.firestore.setDocument, `trademarks/${trademarkId}`, {
        ...data,
      });
      yield put({
        type: actions.EDIT_TRADEMARK_SUCCESS,
        error: "Chỉnh sửa thông tin thành công",
      });
      yield resolve();
    } catch (e) {
      yield put({ type: actions.EDIT_TRADEMARK_ERROR, error: e.message });
      reject(e);
    }
  });
}

export function* deleteTrademarkRequest() {
  yield takeEvery(actions.DELETE_TRADEMARK_REQUEST, function* ({ payload }) {
    const { trademarkId, resolve, reject } = payload;
    try {
      yield call(rsf.firestore.deleteDocument, `trademarks/${trademarkId}`);
      yield put({ type: actions.GET_TRADEMARKS_REQUEST });
      yield resolve();
    } catch (e) {
      yield put({ type: actions.EDIT_TRADEMARK_ERROR, error: e.message });
      reject(e);
    }
  });
}

export default function* rootSaga() {
  yield all([
    fork(getTrademarksRequest),
    fork(getTrademarkRequest),
    fork(addTrademarkRequest),
    fork(editTrademarkRequest),
    fork(deleteTrademarkRequest),
  ]);
}
