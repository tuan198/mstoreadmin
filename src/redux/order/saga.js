import { all, takeEvery, put, fork, call } from "redux-saga/effects";
import actions from "@iso/redux/order/actions";
import rsf from "@iso/config/firebase.config";

export function* getOrdersRequest() {
  yield takeEvery(actions.GET_ORDERS_REQUEST, function* () {
    try {
      const snapshot = yield call(rsf.firestore.getCollection, "invoices");
      let orders = [];
      snapshot.forEach((order) => {
        orders.push({
          ...order.data(),
          id: order.id,
        });
      });
      yield put({ type: actions.GET_ORDERS_SUCCESS, orders: orders });
    } catch (e) {
      yield put({ type: actions.GET_ORDERS_ERROR, error: e.message });
    }
  });
}

export function* getOrderRequest() {
  yield takeEvery(actions.GET_ORDER_REQUEST, function* ({ payload }) {
    const { orderId } = payload;
    try {
      const snapshot = yield call(rsf.firestore.getDocument, `invoices/${orderId}`);
      const order = snapshot.data();
      yield put({ type: actions.GET_ORDER_SUCCESS, order: order });
    } catch (e) {
      yield put({ type: actions.GET_ORDER_ERROR, error: e.message });
    }
  });
}

export function* changeOrderStatusRequest() {
  yield takeEvery(actions.CHANGE_ORDER_STATUS_REQUEST, function* ({ payload }) {
    const { status, orderId, resolve, reject } = payload;
    try {
      yield call(
        rsf.firestore.updateDocument,
        `invoices/${orderId}`,
        "status",
        status
      );
      yield put({ type: actions.CHANGE_ORDER_STATUS_SUCCESS });
      yield resolve();
    } catch (e) {
      yield put({ type: actions.CHANGE_ORDER_STATUS_ERROR, error: e.message });
      yield reject(e);
    }
  });
}

export default function* rootSaga() {
  yield all([
    fork(getOrdersRequest),
    fork(getOrderRequest),
    fork(changeOrderStatusRequest),
  ]);
}
