const actions = {
  GET_ORDERS_REQUEST: "GET_ORDERS_REQUEST",
  GET_ORDERS_SUCCESS: "GET_ORDERS_SUCCESS",
  GET_ORDERS_ERROR: "GET_ORDERS_ERROR",

  GET_ORDER_REQUEST: "GET_ORDER_REQUEST",
  GET_ORDER_SUCCESS: "GET_ORDER_SUCCESS",
  GET_ORDER_ERROR: "GET_ORDER_ERROR",

  CHANGE_ORDER_STATUS_REQUEST: "CHANGE_ORDER_STATUS_REQUEST",
  CHANGE_ORDER_STATUS_SUCCESS: "CHANGE_ORDER_STATUS_SUCCESS",
  CHANGE_ORDER_STATUS_ERROR: "CHANGE_ORDER_STATUS_ERROR",

  getOrders: () => ({
    type: actions.GET_ORDERS_REQUEST,
  }),

  getOrder: (payload) => ({
    type: actions.GET_ORDER_REQUEST,
    payload: payload,
  }),

  changeOrderStatus: (payload) => ({
    type: actions.CHANGE_ORDER_STATUS_REQUEST,
    payload: payload,
  }),
};

export default actions;
