import actions from "@iso/redux/order/actions";

const initialState = {
  orders: [],
  order: {},
  documents: [],
  loading: false,
  error: null,
};

export default function orderReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_ORDERS_REQUEST:
      return {
        ...state,
        loading: true,
        orders: [],
        error: null,
      };
    case actions.GET_ORDERS_SUCCESS:
      return {
        ...state,
        loading: false,
        orders: action.orders,
        error: null,
      };
    case actions.GET_ORDERS_ERROR:
      return {
        ...state,
        loading: false,
        orders: [],
        error: action.error,
      };
    case actions.GET_ORDER_REQUEST:
      return {
        ...state,
        loading: true,
        order: {},
        error: null,
      };
    case actions.GET_ORDER_SUCCESS:
      return {
        ...state,
        loading: false,
        order: action.order,
        error: null,
      };
    case actions.GET_ORDER_ERROR:
      return {
        ...state,
        loading: false,
        order: {},
        error: action.error,
      };

    case actions.CHANGE_ORDER_STATUS_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case actions.CHANGE_ORDER_STATUS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
      };
    case actions.CHANGE_ORDER_STATUS_ERROR:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}
