import { combineReducers } from "redux";
import App from "@iso/redux/app/reducer";
import Auth from "@iso/redux/auth/reducer";
import Mails from "@iso/redux/mail/reducer";
import ThemeSwitcher from "@iso/redux/themeSwitcher/reducer";
import LanguageSwitcher from "@iso/redux/languageSwitcher/reducer";
import profile from "@iso/redux/profile/reducer";
import Product from "@iso/redux/product/reducer";
import Category from "@iso/redux/category/reducer";
import Trademark from "@iso/redux/trademark/reducer";
import User from "@iso/redux/user/reducer";
import Order from "@iso/redux/order/reducer";

export default combineReducers({
  Auth,
  App,
  ThemeSwitcher,
  LanguageSwitcher,
  Mails,
  profile,
  Product,
  Category,
  Trademark,
  User,
  Order,
});
