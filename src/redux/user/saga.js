import { all, takeEvery, put, fork, call } from "redux-saga/effects";
import actions from "@iso/redux/user/actions";
import rsf from "@iso/config/firebase.config";

export function* getUsersRequest() {
  yield takeEvery(actions.GET_USERS_REQUEST, function* () {
    try {
      const snapshot = yield call(rsf.firestore.getCollection, "users");
      let users = [];
      snapshot.forEach((user) => {
        users.push({
          ...user.data(),
          id: user.uid,
        });
      });
      yield put({
        type: actions.GET_USERS_SUCCESS,
        users: users,
        totalUser: snapshot.size,
      });
    } catch (e) {
      yield put({ type: actions.GET_USERS_ERROR, error: e.message });
    }
  });
}

export default function* rootSaga() {
  yield all([fork(getUsersRequest)]);
}
