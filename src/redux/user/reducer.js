import actions from "@iso/redux/user/actions";

const initialState = {
  users: [],
  loading: false,
  error: null,
  totalUser: 0,
};

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_USERS_REQUEST:
      return {
        ...state,
        loading: true,
        users: [],
        error: null,
      };
    case actions.GET_USERS_SUCCESS:
      return {
        ...state,
        loading: false,
        users: action.users,
        error: null,
        totalUser: action.totalUser,
      };
    case actions.GET_USERS_ERROR:
      return {
        ...state,
        loading: false,
        users: [],
        error: action.error,
      };
    default:
      return state;
  }
}
